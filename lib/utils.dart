import 'dart:io' as Io;
import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart' as pp;

import "retroshare.dart" as rs;


List<String> findHashTags(String lookupString) {
  RegExp exp = new RegExp(r"(#(?:[^\x00-\x7F]|\w)+)");
  Iterable<RegExpMatch> matches = exp.allMatches(lookupString);
  List<String> hasTagsList = [];
  if (matches.isNotEmpty) {
    hasTagsList = matches.map((x) => x[0].substring(1)).toList();
  }
  print("Found tags: $hasTagsList");
  return hasTagsList;
}



Future<String> getOrCreateExternalDir([String appendToPath = ""]) async {
  print("Getting or creating download folder.");
  final downloadPath = await getExternalPath() + appendToPath;
  if (await Permission.storage.request().isGranted) {
    await createDir(downloadPath);
    return downloadPath;
  } else {
//    openAppSettings();
    throw Exception("No permission to create the download directory.");
  }
}

Future<bool> createDir(String path) async {
  try {
    var dir = Io.Directory(path);
    bool dirExists = await dir.exists();
    if (!dirExists) {
      print("Creating directory: $path");
      await dir.create(recursive: true);
    }
  } catch (e) {
    print("Error creating directory. $e");
    return false;
  }
  return true;
}

Future<String> getExternalPath() async {
  List extDirectories = await pp.getExternalStorageDirectories();
  Io.Directory externalPath = extDirectories.last;
  for (var extDir in extDirectories) {
    if (!extDir.path.contains('emulated')) {
      externalPath = extDir;
    }
  }
  return externalPath.path;
}


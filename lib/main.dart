import 'dart:ui';

import 'package:elRepoIo/constants.dart' as cnst;
import 'package:elRepoIo/ui/bottommenu/pages/tabs_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sentry/sentry.dart';

import 'constants.dart';
import 'repo.dart' as repo;
import 'ui.dart' as ui;
import 'utils.dart' as utils;
import 'intl.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

User user = new User();

final SentryClient _sentry = SentryClient(
    dsn:
        "https://f3dadd9fc2324d8fbf3655bfafc3895c@o448455.ingest.sentry.io/5429863");

bool get isInDebugMode {
  // Assume you're in production mode.
  bool inDebugMode = false;

  // Assert expressions are only evaluated during development. They are ignored
  // in production. Therefore, this code only sets `inDebugMode` to true
  // in a development environment.
  assert(inDebugMode = true);

  return inDebugMode;
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  // Print the exception to the console.
  print('Caught error: $error');
  if (isInDebugMode) {
    // Print the full stacktrace in debug mode.
    print(stackTrace);
  } else {
    // Print the full stacktrace
    print(stackTrace);
    // and send the Exception and Stacktrace to Sentry in Production mode.
    _sentry.captureException(exception: error, stackTrace: stackTrace);
  }
}

void main() async {
  runZonedGuarded<Future<void>>(
    () async {
      runApp(MaterialApp(
        localizationsDelegates: [
          const SimpleLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''),
          const Locale('es', ''),
        ],
        theme: ThemeData(
         // brightness: Brightness.light,
          primaryColor: ACCENT_COLOR,
          accentColor: LOCAL_COLOR,
          textTheme: TextTheme(
            display1:
                TextStyle(color: BTN_FONT_COLOR, fontWeight: FontWeight.bold),
            display2: TextStyle(
                color: BTN_FONT_COLOR,
                fontWeight: FontWeight.bold,
                fontSize: 20),
            button:
                TextStyle(color: REMOTE_COLOR, fontWeight: FontWeight.normal),
            headline:
                TextStyle(color: LOCAL_COLOR, fontWeight: FontWeight.normal),
          ),
         // visualDensity: VisualDensity.adaptivePlatformDensity,
          inputDecorationTheme: InputDecorationTheme(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: NEUTRAL_COLOR.withOpacity(.4),
              ),
            ),
          ),
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),
          '/signup': (context) => ui.SignUpScreen(),
          '/onboarding': (context) => ui.OnBoarding(),
          '/login': (context) => ui.LoginScreen(),
          '/status': (context) => ui.StatusScreen(),
          '/main': (context) => DefaultTabController(
                length: 3,
                child: Scaffold(
                  appBar: AppBar(
                    title: Text('elRepo.io'),
                    elevation: 0.0,
                  ),
                  drawer: Drawer(
                    child: ListView(
                      // Important: Remove any padding from the ListView.
                      padding: EdgeInsets.zero,
                      children: <Widget>[
                        DrawerHeader(
//                      child: Text(''),
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                          ),
                        ),
                        ListTile(
                          title: Text('App Status'),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.pushNamed(context, '/status');
                          },
                        ),
                      ],
                    ),
                  ),
                  body: TabsPage(),
                ),
              ),
        },
        onGenerateRoute: (RouteSettings settings) {
          print('build route for ${settings.name} using ${settings.arguments}');
          var routes = <String, WidgetBuilder>{
            "/postdetails": (context) =>
                ui.PostDetailsScreen(settings.arguments),
            "/browsetag": (context) => ui.BrowseTagScreen(settings.arguments),
            "/comment": (context) => ui.CommentScreen(settings.arguments),
            "/identityDetails": (context) =>
                ui.IdentityDetailsScreen(settings.arguments),
          };
          WidgetBuilder builder = routes[settings.name];

          return MaterialPageRoute(builder: (context) => builder(context));
        },
      ));
    },
    (Object error, StackTrace stackTrace) {
      _reportError(error, stackTrace);
    },
  );
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<List> postHeaders;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    super.initState();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    _showKeepAliveNotification();

    utils.getOrCreateExternalDir();

    repo.startRetroshare().then((isRunning) {
      if (isRunning) {
        redirectOnAccountStatus(context);
      } else {
        // TODO(nicoechaniz): expose the failure to start RS service to the user interface
        throw Exception("RS is not running");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(child: Image.asset("assets/logo.png")),
          Text(
            "elRepo.io\n",
            style: Theme.of(context).textTheme.display1,
          ),
        ],
      ),
    );
  }

  Future _showKeepAliveNotification() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'elRepo.io',
        'keep alive notification',
        'this notification keeps elRepo.io alive',
        playSound: false,
        importance: Importance.Max,
        priority: Priority.Max);
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'elRepo.io',
      'this notification keeps elRepo.io alive',
      platformChannelSpecifics,
      payload: 'this is the payload',
    );
  }
}

void redirectOnAccountStatus(BuildContext context) async {
  final accountStatus = await repo.getAccountStatus();
  switch (accountStatus) {
    case cnst.AccountStatus.isLoggedIn:
      Navigator.pushReplacementNamed(context, '/main');
      break;
    case cnst.AccountStatus.hasLocation:
      Navigator.pushReplacementNamed(context, '/login');
      break;
    case cnst.AccountStatus.hasNoLocation:
      Navigator.pushReplacementNamed(context, '/signup');
      break;
  }
}

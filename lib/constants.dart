import 'package:flutter/material.dart';

const APP_DIR = 'net.altermundi.elrepoio';
const API_VERSION = "0.0.13";
const FORUM_PREPEND = "elRepo.io_";
const SUMMARY_LENGTH = 100;
//final BACKGROUND_COLOR = Colors.green[50];
final BACKGROUND_COLOR = Color(0xffe5ffe4);
final FULL_PAGE_BACKGROUND_COLOR = Colors.white;
//const LOCAL_COLOR = Colors.green;
const LOCAL_COLOR = Color(0xff75a478);
const ACCENT_COLOR = Color(0xffa5d6a7);
const REMOTE_COLOR = Colors.purple;
const NEUTRAL_COLOR = Colors.black54;
const BTN_FONT_COLOR = Color(0xff263238);

class AccountStatus {
  static const String isLoggedIn = "is logged in";
  static const String hasLocation = "has location";
  static const String hasNoLocation = "has no location";
}

class MarkupTypes {
  static const String plain = "plain";
  static const String markdown = "markdown";
  static const String html = "html";
}

class ContentTypes {
  static const String ref = "ref";
  static const String image = "image";
  static const String audio = "audio";
  static const String video = "video";
  static const String document = "document";
  static const String text = "text";
  static const String file = "file";
}

class PostRoles {
  static const String post = "post";
  static const String comment = "comment";
}

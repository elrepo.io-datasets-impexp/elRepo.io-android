import 'dart:convert';
import 'package:path/path.dart' as path;
import 'package:mime/mime.dart';

import 'constants.dart' as cnst;
import 'retroshare.dart' as rs;
import 'utils.dart' as utils;
import 'models.dart' as models;
import 'ui.dart'
    as ui; //TODO(nicoechaniz): remove all dependencies on ui when possible

List forumTypes = ["CONTENT", "TAG", "USER", "BOOKMARKS"];

Future<bool> startRetroshare() async {
  var isRunning = await rs.isRetroshareRunning();
  if (!isRunning) {
    isRunning = await rs.startRetroshare();
  }
  return isRunning;
}

Future<String> getAccountStatus() async {
  final isLoggedIn = await rs.RsLoginHelper.isLoggedIn();
  if (isLoggedIn) {
    print("Is logged in");
    return cnst.AccountStatus.isLoggedIn;
  } else {
    if (await rs.RsLoginHelper.hasLocation()) {
      print("Has Location -> Login");
      return cnst.AccountStatus.hasLocation;
    } else {
      print("Does Not Have Location -> SignUp");
      return cnst.AccountStatus.hasNoLocation;
    }
  }
}

Future<dynamic> subscribeToRepoForums() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final elRepoForums = allForums
      .where((i) => i["mGroupName"] == "elRepo_${cnst.API_VERSION}_")
      .toList();
  print("Found elRepo.io forums $elRepoForums");
  Map forum;
  for (forum in elRepoForums) {
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values
    rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
  }
}

Future<int> login(String password, [Map location]) async {
  if (location == null) {
    location = await rs.RsLoginHelper.getDefaultLocation();
    print('logging in $location');
  }
  final responseStatus = await rs.RsLoginHelper.login(location, password);
  return responseStatus;
}

Future<Map> signUp(String password, String locationName) async {
  print('creating account for $locationName');
  final location =
      await rs.RsLoginHelper.createLocation(locationName, password);
  print("Location: $location");
  return location;
}

Future<bool> prepareSystem() async {
  //TODO(nicoechaniz): check if already friend
  await rs.befriendTier1(hostname: "margaretsanger");

  final downloadDir = await utils.getOrCreateExternalDir("/Download");
  final partialsDir = await utils.getOrCreateExternalDir("/Partials");

  print(
      "Setting Retroshare directories. \n Download and shared: $downloadDir\n Partials: $partialsDir");
  rs.RsFiles.setDownloadDirectory(downloadDir);
  rs.RsFiles.addSharedRepoDirectory(downloadDir);
  rs.RsFiles.setPartialsDirectory(partialsDir);

  await subscribeToRepoForums();
  rs.RsGxsForum.requestSynchronization();
  return true;
}

String getForumName(String forumType, [String label = ""]) {
  if (!forumTypes.contains(forumType)) {
    throw Exception("invalid forum type");
  }
  if (label != "") {
    label = "_$label";
  }
  return "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label";
}

Future<String> findOrCreateRepoForum(
    [String forumType = "CONTENT", String label = ""]) async {
  if (!forumTypes.contains(forumType)) {
    throw Exception("invalid forum type");
  }
  if (label != "") {
    label = "_$label";
  }
  String forumName =
      "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_$forumType$label";
  print("Find or create $forumName");
  var result = await rs.RsGxsForum.getForumsSummaries();
  var elRepoForums = result.where((i) => i["mGroupName"] == forumName).toList();
  var forumId;
  if (elRepoForums.length > 0) {
    print("Found existing elRepo.io forums");
    forumId = elRepoForums[0]["mGroupId"];
  } else {
    forumId = await rs.RsGxsForum.createForumV2(forumName);
    print("No elRepo.io forums found, creating: $forumName");
    rs.RsGxsForum.subscribeToForum(forumId, true);
  }
  return forumId;
}

String summaryFromBody(bodyText) {
  String summary;
  if (bodyText.length > cnst.SUMMARY_LENGTH) {
    summary = bodyText.substring(0, cnst.SUMMARY_LENGTH);
    summary = "$summary...";
  } else {
    summary = bodyText;
  }
  return summary;
}

String getContentTypeFromPath(String filePath) {
  final fileName = path.basename(filePath);
  final fileType = lookupMimeType(fileName);
  var fileRootType = "";
  if (fileType is String) {
    fileRootType = fileType.split("/")[0];
  }
/*TODO(nicoechaniz): we should check if cns.ContentTypes has the attribute to avoid depending on ui.dart here,
 but I haven't figured out how to introspect a dart object */
  if (ui.contentTypeIcons.containsKey(fileRootType)) {
    return fileRootType;
  } else
    return cnst.ContentTypes.text;
}

Future<models.PublishData> publishPost(models.PostData postData) async {
  List fileHashAndLink = [];
  if (postData.filePath != "") {
    final hashingFile = await rs.RsFiles.extraFileHash(postData.filePath);

    if (hashingFile == true) {
      print("Hashing file ${postData.filePath}");
      fileHashAndLink = await rs.waitForFileHashAndLink(postData.filePath);
      postData.mBody.payloadLinks.add(models.PayloadLink(
          postData.filename, fileHashAndLink[0], fileHashAndLink[1]));
      print("Hashing done for ${postData.filePath}. Result: $fileHashAndLink");
      postData.metadata.contentType = getContentTypeFromPath(postData.filePath);
    }
  }

  if (postData.metadata.summary == null) {
    postData.metadata.summary = summaryFromBody(postData.mBody.text);
  }

  final forumId = await findOrCreateRepoForum();
  final msgId = await rs.RsGxsForum.createPost(
      forumId,
      jsonEncode(postData.metadata),
      jsonEncode(postData.mBody),
      rs.authIdentityId);

  final hashTags = utils.findHashTags(postData.mBody.text);
  final linkPostMetadata = jsonEncode(
      models.PostMetadata.generateLinkPostMetadata(
          postData.metadata, msgId, forumId));
  String tag;
  for (tag in hashTags) {
// we add the reference to the real post
    final tagForumId = await findOrCreateRepoForum("TAG", tag);
    print("Creating post in $tagForumId");
// we don't save the message body in a linkPost
    rs.RsGxsForum.createPost(
        tagForumId, linkPostMetadata, "", rs.authIdentityId);
  }
// we also create a linkPost in the user forum to keep track of content published.
  final userForumId = await findOrCreateRepoForum("USER", rs.authIdentityId);
  rs.RsGxsForum.createPost(
      userForumId, linkPostMetadata, "", rs.authIdentityId);

  models.PublishData publishData =
      new models.PublishData(forumId, msgId, hashTags);

  return publishData;
}

Future<List> getTagNames() async {
  print("Getting tag names");
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final tagForums = allForums
      .where((i) => i["mGroupName"]
          .toString()
          .startsWith("${cnst.FORUM_PREPEND}${cnst.API_VERSION}_TAG_"))
      .toList();
  print("Tag forums $tagForums");

  Map forum;
  var allTags = new Set();
  String tagName;
  String groupName;
  for (forum in tagForums) {
    // TODO(): check to only subscribe when needed.
    if (forum["mSubscribeFlags"] == 8)
      await rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
    groupName = forum["mGroupName"].toString();
    tagName =
        groupName.substring(groupName.lastIndexOf("_TAG_") + '_TAG_'.length);
    allTags.add(tagName);
  }
  print("Found tags: $allTags");
  final tagsList = allTags.toList();
  print("tags list is $tagsList");
  return (tagsList);
}

void syncForums() {
  // TODO(nicoechaniz): this is not currently working as expected from the RetroShare end.
  // re-visit once the functionality is improved in RetroShare.
  rs.RsGxsForum.requestSynchronization();
}

Future<List> getFollowedAuthorsDetails() async {
  print("Getting authors data");
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  print("All forums: $allForums");
  final followingForums = allForums
      .where((i) =>
          i["mGroupName"]
              .toString()
              .startsWith("${cnst.FORUM_PREPEND}${cnst.API_VERSION}_USER_") &&
          i["mSubscribeFlags"] == 4)
      .toList();
  print("Followed User forums: $followingForums");
  String authorId;
  List<Map<String, dynamic>> authorsDetails = [];
  String groupName;
  Map forum;
  for (forum in followingForums) {
    groupName = forum["mGroupName"].toString();
    authorId =
        groupName.substring(groupName.lastIndexOf("_USER_") + '_USER_'.length);
    print("The author id is: $authorId");
    authorsDetails.add(await rs.RsIdentity.getIdDetails(authorId));
  }

  print("Author details found: $authorsDetails");
  return authorsDetails;
}

Future<String> getAuthorName(String authorId) async {
  final identityDetails = await rs.RsIdentity.getIdDetails(authorId);
  return identityDetails["mNickname"];
}

Future<String> createComment(String forumId, String metadata,
    String commentBody, String parentPostId) async {
  final msgId = await rs.RsGxsForum.createPost(
      forumId, metadata, commentBody, rs.authIdentityId, parentPostId);
  return msgId;
}


Future<Map> getUserForum(String identityId) async {
  final userForumName = getForumName("USER", identityId);
  final requestedForums = await getForumsByName(userForumName);
  // there should be only one forum for each user, so we pick the first result
  final userForum = requestedForums[0];
  return userForum;
}

Future<List> getForumMetadata(String forumId) {
  return rs.RsGxsForum.getForumMsgMetaData(forumId);
}

void subscribeToForum(Map forum){
//void followUser(String identityId){
  rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
  rs.RsGxsForum.requestSynchronization();
}

Future<Map> getIdDetails (String identityId) {
  final identityDetails = rs.RsIdentity.getIdDetails(identityId);
  return identityDetails;
}

Future<List> getForumsByName(String forumName) async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final allForumNames = allForums.map((i) => i["mGroupName"]).toList();
  print("All known forum names: $allForumNames");
  final requestedForums =
  allForums.where((i) => i["mGroupName"] == forumName).toList();
  return requestedForums;
}

Future<List> getPostHeaders(
    [String forumType = "CONTENT", String label = ""]) async {
  final requestedForumName = getForumName(forumType, label);
  Map forum;
  List allPosts = [];
  final requestedForums = await getForumsByName(requestedForumName);
  for (forum in requestedForums) {
    print('Found forum: ${forum["mGroupName"]} / id: ${forum["mGroupId"]}');
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values before trying to subscribe
    rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
    var posts = await rs.RsGxsForum.getForumMsgMetaData(forum["mGroupId"]);
    allPosts.addAll(posts.where((post) {
      models.PostMetadata postMetadata =
      models.PostMetadata.fromJsonString(post["mMsgName"]);
      return postMetadata.role == cnst.PostRoles.post;
    }));
  }
  return allPosts;
}

getRealPostId(postMetadata) {
//  print('Link Post : $jsonMetadata');
//    final linkPostMetadata = models.PostMetadata.fromJsonString(jsonMetadata);

  try {
    final realPostId = postMetadata.referred.split(" ")[1];
    return realPostId;
  } catch (error) {
    print("Could not retrieve original post: $error");
    return null;
  }
}


List bookmarksCache;
List selfPostsCache;
Duration cacheDuration = Duration(seconds: 30);
DateTime bookmarksLastRefresh;
DateTime selfPostsLastRefresh;

Future<List<dynamic>> getBookmarkedIds() async {
  if (bookmarksLastRefresh == null) {
    bookmarksLastRefresh = DateTime.now();
  }
  Duration timeDelta = DateTime.now().difference(bookmarksLastRefresh);
  if (timeDelta < cacheDuration && bookmarksCache != null) {
    print("------------------ return bookmarks from cache");
    return bookmarksCache;
  }
  List bookmarkedIds;
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final bookmarkForumName =
      "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_BOOKMARKS_${rs.authIdentityId}";
  final bookmarksForum =
  allForums.where((i) => i["mGroupName"] == bookmarkForumName).toList();
  if (bookmarksForum.length > 0) {
    final bookmarkedMessages =
    await rs.RsGxsForum.getForumMsgMetaData(bookmarksForum[0]["mGroupId"]);
    bookmarkedIds = bookmarkedMessages
        .map((item) =>
        getRealPostId(models.PostMetadata.fromJsonString(item["mMsgName"])))
        .toList();
  }
  bookmarksCache = bookmarkedIds;
  bookmarksLastRefresh = DateTime.now();
  return bookmarkedIds;
}

Future<List<dynamic>> getSelfPostIds() async {
  if (selfPostsLastRefresh == null) {
    selfPostsLastRefresh = DateTime.now();
  }
  Duration timeDelta = DateTime.now().difference(selfPostsLastRefresh);
  if (timeDelta < cacheDuration && selfPostsCache != null) {
    return selfPostsCache;
  }
  List selfPostsIds;
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  print("Fetching User posts");
  final userForumName =
      "${cnst.FORUM_PREPEND}${cnst.API_VERSION}_USER_${rs.authIdentityId}";
  final userForum =
  allForums.where((i) => i["mGroupName"] == userForumName).toList();
  if (userForum.length > 0) {
    final userMessages =
    await rs.RsGxsForum.getForumMsgMetaData(userForum[0]["mGroupId"]);
    selfPostsIds = userMessages
        .map((item) =>
        getRealPostId(models.PostMetadata.fromJsonString(item["mMsgName"])))
        .toList();
  }
  selfPostsCache = selfPostsIds;
  selfPostsLastRefresh = DateTime.now();
  return selfPostsIds;
}

Future<String> getPathIfExists(hash) async {
  final fileInfo = await rs.RsFiles.alreadyHaveFile(hash);
  if (fileInfo is Map) {
    return fileInfo["path"];
  } else
    return null;
}

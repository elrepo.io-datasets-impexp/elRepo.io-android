library ui;

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'retroshare.dart' as rs;
import 'dart:convert';
import 'utils.dart' as utils;
import 'repo.dart' as repo;
import 'package:share/share.dart';
import 'package:file_picker/file_picker.dart';
import 'models.dart' as models;
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as pp;
import 'dart:io' as Io;
import 'constants.dart';
import 'package:open_file/open_file.dart';
import 'intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:image_picker/image_picker.dart';

part "ui/browse.dart";
part "ui/postdetail.dart";
part "ui/publish.dart";
part "ui/login.dart";
part "ui/signup.dart";
part "ui/status.dart";
part "ui/settings.dart";
part "ui/tags.dart";
part "ui/browsetag.dart";
part "ui/bookmarks.dart";
part "ui/comment.dart";
part "ui/identityDetails.dart";
part "ui/following.dart";
part "ui/editprofile.dart";
part "ui/settingsprofile.dart";
part "ui/profile.dart";
part "ui/onboarding.dart";

bool notNull(Object o) => o != null;

class PostArguments {
  final String forumId;
  final String postId;
  final bool isLinkPost;
  PostArguments(this.forumId, this.postId, [this.isLinkPost]);
}

class TagArguments {
  final String tagName;
  TagArguments(this.tagName);
}

class IdentityArguments {
  final String identityId;
  IdentityArguments(this.identityId);
}

Widget loadingBox([String loadingMsg = 'Loading ...']) {
  return Center(
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
          new Padding(
            padding: EdgeInsets.only(top: 15),
            child: Text(loadingMsg),
          )
        ]),
  );
}

Future showLoadingDialog(context, [String loadingMsg = 'Loading ...']) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return SimpleDialog(
        children: <Widget>[
          Center(
              child: Column(children: [
            CircularProgressIndicator(),
            SizedBox(
              height: 10,
            ),
            Text(loadingMsg, style: TextStyle(color: Colors.green[600]))
          ]))
        ],
      );
    },
  );
}

defaultMargin() {
  return EdgeInsets.all(15.0);
}

Future<bool> isBookmarked(msgId) async {
  final bookmarkedIds = await repo.getBookmarkedIds();
  if (bookmarkedIds != null && bookmarkedIds.contains(msgId))
    return true;
  else
    return false;
}

Future<bool> isSelfPost(msgId) async {
  final selfPostIds = await repo.getSelfPostIds();
  if (selfPostIds != null && selfPostIds.contains(msgId))
    return true;
  else
    return false;
}

Future<bool> isLocal(msgId) async {
  return (await (isBookmarked(msgId)) || await (isSelfPost(msgId)));
}

Widget getPostIcon(models.PostMetadata postMetadata, msgId,
    [bool isLinkPost = false]) {
  Icon postIcon;
  var realMsgId;

  if (isLinkPost) {
    realMsgId = repo.getRealPostId(postMetadata);
  } else {
    realMsgId = msgId;
  }

  return FutureBuilder(
    future: isLocal(realMsgId),
    builder: (context, snapshot) {
      if (snapshot.hasError) {
        print('Error: ${snapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      } else if (snapshot.hasData == false) {
        return Icon(Icons.hourglass_empty, color: NEUTRAL_COLOR);
      }
      switch (snapshot.data) {
        case (true):
          {
            postIcon = Icon(getContentTypeIcon(postMetadata.contentType),
                color: LOCAL_COLOR);
            break;
          }
        case (false):
          postIcon = Icon(getContentTypeIcon(postMetadata.contentType),
              color: REMOTE_COLOR);
      }
      return postIcon;
    },
  );
}

Widget postTeasersWidget(Future<List> postsHeaders, {bool linkPosts = false}) {
  return new FutureBuilder(
    future: postsHeaders,
    builder: (context, postsSnapshot) {
      if (postsSnapshot.hasError) {
        print('Error: ${postsSnapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      }
      // TODO(nicoechaniz): there is some implementation error or a bug? which makes snapshot.data on the first run
      //  to be that of the previously showing view/tab, so we check the type (hacky solution)
      else if (postsSnapshot.hasData == false ||
          (postsSnapshot.data.length != 0 &&
              !(postsSnapshot.data[0] is Map<String, dynamic>))) {
        return loadingBox();
      } else if (postsSnapshot.data == null || postsSnapshot.data.length == 0) {
        return Visibility(visible: false, child: Text(""));
      } else {
        return ListView.builder(
          itemCount: postsSnapshot.data.length,
          itemBuilder: (context, index) {
            final data = postsSnapshot.data[index];
            models.PostMetadata postMetadata =
                models.PostMetadata.fromJsonString(data["mMsgName"]);

            return Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white),
                child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    leading: Container(
                      padding: EdgeInsets.only(right: 12.0),
                      decoration: new BoxDecoration(
                          border: new Border(
                              right: new BorderSide(
                                  width: 1.0, color: Colors.green[200]))),
/*
                      child: Icon(getContentTypeIcon(postMetadata.contentType),
                          color: getStatusColor(bookmarkedIds, data["mMsgId"])),
*/
                      child:
                          getPostIcon(postMetadata, data["mMsgId"], linkPosts),
                    ),
                    onTap: () {
                      // if it is a linkPost, we need to redirect to the CONTENT forum post on click.
                      Navigator.pushNamed(context, "/postdetails",
                          arguments: PostArguments(
                              data["mGroupId"], data["mMsgId"], linkPosts));
                    },
                    title: Text(
                        postMetadata.title != null ? postMetadata.title : ""),
                    subtitle: Text(postMetadata.summary != null
                        ? postMetadata.summary
                        : "")),
              ),
            );
          },
        );
      }
    },
  );
}

IconData getContentTypeIcon(String contentType) {
  if (contentTypeIcons.containsKey(contentType)) {
    return contentTypeIcons[contentType];
  } else {
    // the default icon is the file icon for unknown types
    return contentTypeIcons[ContentTypes.file];
  }
}

Map<String, IconData> contentTypeIcons = {
  ContentTypes.image: Icons.image,
  ContentTypes.audio: Icons.audiotrack,
  ContentTypes.video: Icons.local_movies,
  ContentTypes.document: Icons.insert_drive_file,
  ContentTypes.file: Icons.attach_file,
  ContentTypes.text: Icons.format_align_left
};

import 'dart:convert';
import 'constants.dart' as cnst;

class PayloadLink {
  String filename = '';
  String hash = '';
  String link = '';
  // TODO(nicoechaniz): hash can be extracted from the link. Implement that where needed and remove hash from here
  Map<String, dynamic> toJson() =>
      {'filename': filename, 'hash': hash, 'link': link};

  PayloadLink(this.filename, this.hash, this.link);
}

class PostBody {
  String text = '';
  List<PayloadLink> payloadLinks = [];

  Map<String, dynamic> toJson() => {'text': text, 'payloadLinks': payloadLinks};
}

class PostMetadata {
  String title;
  String summary;
  String markup;
  String contentType;
  String role;
  String referred;

  PostMetadata(
      {this.title,
      this.summary,
      this.markup,
      this.contentType,
      this.referred,
      this.role});

  // get metadata for a linkPost
  PostMetadata.generateLinkPostMetadata(
      PostMetadata postMetadata, String msgId, String forumId)
      : this(
            title: postMetadata.title,
            summary: postMetadata.summary,
            markup: postMetadata.markup,
            contentType: postMetadata.contentType,
            role: postMetadata.role,
            referred:
                "$forumId $msgId"); // reference to the actual content post

  Map<String, dynamic> toJson() => {
        'title': title,
        'summary': summary,
        'markup': markup,
        'contentType': contentType,
        'role': role,
        'referred': referred
      };

  factory PostMetadata.fromJsonString(String jsonString) {
    final json = jsonDecode(jsonString);
    return PostMetadata(
        title: json['title'],
        summary: json['summary'],
        markup: json['markup'],
        contentType: json['contentType'],
        role: json['role'],
        referred: json['referred']);
  }
}

class PostData {
  PostMetadata metadata = new PostMetadata(
      markup: cnst.MarkupTypes.plain,
      contentType: cnst.ContentTypes.text,
      role: cnst.PostRoles.post);
  PostBody mBody = new PostBody();
  String filePath = '';
  String filename = '';
}

class PublishData {
  String msgId;
  String forumId;
  List hashTags;

  PublishData(this.msgId, this.forumId, this.hashTags);
}


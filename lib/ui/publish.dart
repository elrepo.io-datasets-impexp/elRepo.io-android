/// lib/ui/publish.dart
part of ui;

class PublishScreen extends StatelessWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => PublishScreen(),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      body: new Container(
        //margin: defaultMargin(),
        child: new ListView(children: <Widget>[new PublishForm()]),
      ),
    );
  }
}

class PublishForm extends StatefulWidget {
  PublishForm({Key key}) : super(key: key);

  @override
  _PublishFormState createState() => _PublishFormState();
}

class _PublishFormState extends State<PublishForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    models.PostData postData = new models.PostData();
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(labelText: IntlMsg.of(context).title),
              validator: (value) {
                if (value.isEmpty)
                  return IntlMsg.of(context).titleCannotBeEmpty;
                postData.metadata.title = value;
                return null;
              },
            ),
            TextFormField(
              maxLines: 3,
              decoration: InputDecoration(
                  alignLabelWithHint: true,
                  labelText: IntlMsg.of(context).description),
              validator: (value) {
                postData.mBody.text = value;
                return null;
              },
            ),
            RaisedButton.icon(
              onPressed: () async {
                postData.filePath = await FilePicker.getFilePath();
                postData.filename = path.basename(postData.filePath);
              },
              label: Text(IntlMsg.of(context).chooseFile,
                style: TextStyle(color: FULL_PAGE_BACKGROUND_COLOR,),),
              icon: Icon(Icons.upload_rounded,color: LOCAL_COLOR,),
              color: ACCENT_COLOR,
              textColor: Colors.white
              ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 32),
                child: RaisedButton(
                  onPressed: () async {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                    final form = _formKey.currentState;
                    if (form.validate()) {
                      form.save();
                      print(
                          "Post form received file path: ${postData.filePath}");
                      showLoadingDialog(
                          context, IntlMsg.of(context).publishingContent);

                      repo.publishPost(postData).then((publishData) {
                        form.reset();
                        postData = new models.PostData();
                        FilePicker.clearTemporaryFiles();

                        Navigator.pop(context); //pop dialog
                        final snackBar = SnackBar(
                          content: Text(IntlMsg.of(context).contentPublished),
                          duration: Duration(seconds: 6),
                          action: SnackBarAction(
                            label: IntlMsg.of(context).checkItOut,
                            onPressed: () {
                              Navigator.pushNamed(context, "/postdetails",
                                  arguments: PostArguments(publishData.forumId,
                                      publishData.msgId, false));
                            },
                          ),
                        );
                        Scaffold.of(context).showSnackBar(snackBar);
                        print(
                            "post Published on forum ${publishData.forumId}\n with id: ${publishData.msgId}\n and tags: ${publishData.hashTags}");
                      });
                    }
                  },
                  child: Center(child: Text(IntlMsg.of(context).publish)),
                  color: LOCAL_COLOR,
                  textColor: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

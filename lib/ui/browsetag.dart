part of ui;

class BrowseTagScreen extends StatefulWidget {
  final TagArguments args;
  BrowseTagScreen(this.args);

  @override
  _BrowseTagScreenState createState() => _BrowseTagScreenState();
}

class _BrowseTagScreenState extends State<BrowseTagScreen> {
  Future<List> postHeaders;

  @override
  void initState() {
    postHeaders = repo.getPostHeaders("TAG", widget.args.tagName);
    repo.syncForums();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: postTeasersWidget(postHeaders, linkPosts: true),
    );
  }
}

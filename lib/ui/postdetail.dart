part of ui;

class PostDetailsScreen extends StatefulWidget {
  final PostArguments args;
  PostDetailsScreen(this.args);

  @override
  _PostDetailsScreenState createState() => _PostDetailsScreenState();
}

class _PostDetailsScreenState extends State<PostDetailsScreen> {
  Future<List> postDetails;
  Timer downloadProgressTimer;
  String downloadingHash;
  bool fileExists = false;
  bool hasPayload = false;
  bool downloadQueued = false;
  double downloadProgress = 0.0;
  Future<String> filePath;
  String authorName = "";
  Future<List> postComments;

  @override
  void initState() {
    postDetails = getPostDetails(
        widget.args.forumId, widget.args.postId, widget.args.isLinkPost);
    super.initState();
  }

  @override
  void dispose() {
    downloadProgressTimer?.cancel();
    super.dispose();
  }

  void startDownloadProgressTimer() {
    downloadProgressTimer = Timer.periodic(
      Duration(seconds: 1),
      downloadProgressHandler,
    );
  }

  void downloadProgressHandler(Timer timer) async {
    var progress = 0.0;
    var hashes = await rs.RsFiles.fileDownloads();
    if (hashes.length > 0) {
      for (var hash in hashes) {
        if (hash == downloadingHash) {
          var downloadDetails = await rs.RsFiles.fileDetails(hash);
          progress = downloadDetails["avail"]["xint64"] /
              downloadDetails["size"]["xint64"];
          print("progress for $hash: $progress");
        }
      }
    }

    if (timer.isActive) {
      if (mounted) setState(() => downloadProgress = progress);

      if (downloadProgress >= 1.0) {
        print("download finished.");
        downloadProgress = 1.0;

        // it takes a while for the downloaded file to be copied to it's final
        // location by Retroshare so we keep the timer up until it's done
        filePath = repo.getPathIfExists(downloadingHash);
        filePath.then((path) async {
          // the file is already available on this device
          if (path != null) {
            timer.cancel();
            if (fileExists != true) {
              if (mounted) setState(() => fileExists = true);
            }
          }
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: postDetailsWidget(),
    );
  }

  Widget postDetailsWidget() {
    models.PostBody postBody = new models.PostBody();
    models.PostMetadata postMetadata;
    String postId;
    String forumId;
    IconData contentTypeIcon;

    return FutureBuilder(
      future: postDetails,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print('Error: ${snapshot.error}');
          return Center(
            child: Text(IntlMsg.of(context).cannotLoadContent),
          );
        } else if (snapshot.hasData == false || snapshot.data.length == 0) {
          return loadingBox();
        } else {
          final data = snapshot.data[0];
          postId = data["mMeta"]["mMsgId"];
          try {
            postMetadata =
                models.PostMetadata.fromJsonString(data["mMeta"]["mMsgName"]);
          } catch (error) {
            print("Error parsing metadata Json: $error");
          }
          try {
            Map jsonBody = jsonDecode(data["mMsg"]);
            postBody.text = jsonBody["text"];
            if (jsonBody["payloadLinks"].length > 0) {
              Map linkData;
              for (linkData in jsonBody["payloadLinks"]) {
                postBody.payloadLinks.add(models.PayloadLink(
                    linkData["filename"], linkData["hash"], linkData["link"]));
              }
            }
          } catch (error) {
            print("Error parsing body Json: $error");
            // this may be a post originated from another client
            // we are choosing to show it but we could ignore it.
            postBody.text = data["mMsg"];
            postBody.payloadLinks = [];
          }
          final authorId = data["mMeta"]["mAuthorId"];
          final identityDetails = rs.RsIdentity.getIdDetails(authorId);
          identityDetails.then((details) {
            if (authorName == "") {
              if (mounted) setState(() => authorName = details["mNickname"]);
            }
          });

          if (widget.args.isLinkPost)
            forumId = data["mMeta"]["mGroupId"];
          else
            forumId = widget.args.forumId;

          if (postComments == null) {
            postComments = rs.RsGxsForum.getChildPosts(forumId, postId);
          }

          if (postBody.payloadLinks.length > 0) {
            hasPayload = true;
            contentTypeIcon = getContentTypeIcon(postMetadata.contentType);
          } else {
            // if there's no associated file, we consider the payload to be the inline text
            contentTypeIcon = Icons.format_align_left;
          }
          if (hasPayload) {
            final hash = postBody.payloadLinks[0].hash;
            final filePath = repo.getPathIfExists(hash);

            filePath.then((path) async {
              // the file is already available on this device
              if (path != null) {
                if (fileExists != true) {
                  if (mounted) setState(() => fileExists = true);
                }
              }
              // else check if it's being downloaded
              else {
                var hashes = await rs.RsFiles.fileDownloads();
                if (hashes.contains(hash)) {
                  downloadingHash = hash;
                  if (downloadProgress < 1.0 && downloadProgressTimer == null) {
                    startDownloadProgressTimer();
                  }
                  if (mounted) setState(() => downloadQueued = true);
                }
              }
            });
          }

          return new Container(
              margin: defaultMargin(),
              child: new ListView(
                  children: <Widget>[
                new ListTile(
                  leading: Container(
                      padding: EdgeInsets.only(right: 5.0),
                      decoration: new BoxDecoration(),
                      child: Icon(contentTypeIcon,
                          color: NEUTRAL_COLOR, size: 48)),
                  title: Text(
                      postMetadata.title != null ? postMetadata.title : "",
                      style: Theme.of(context).textTheme.subtitle1),
                  subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, "/identityDetails",
                                arguments: IdentityArguments(authorId));
                          },
                          child: Text(authorName,
                              style: TextStyle(color: LOCAL_COLOR)),
                        ),
                        Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new IconButton(
                                  icon: new Icon(Icons.share,
                                      color: Colors.black54),
                                  onPressed: () async {
                                    String shareText =
                                        "${postMetadata.title}\n ${postMetadata.summary}";
                                    if (hasPayload && fileExists) {
                                      final filePath =
                                          await repo.getPathIfExists(
                                              postBody.payloadLinks[0].hash);
                                      Share.shareFiles([filePath],
                                          text: shareText);
                                    } else {
                                      Share.share(shareText);
                                    }
                                  }),
                              (hasPayload && !fileExists)
                                  ? new DownloadWidget(postMetadata, postId,
                                      postBody, widget.args.forumId)
                                  : null,
                              (hasPayload && fileExists)
                                  ? new OpenPayloadWidget(repo.getPathIfExists(
                                      postBody.payloadLinks[0].hash))
                                  : null,
                            ].where(notNull).toList())
                      ]),
                ),
                Visibility(
                    // TODO(nicoechaniz): this works OK for showing the progressbar when visiting a content
                    // but when a download is started on a content, this does not get triggered and the bar is invisible
                    visible: (hasPayload &&
                        downloadQueued &&
                        downloadProgress < 1.0),
                    child: new LinearPercentIndicator(
                      linearStrokeCap: LinearStrokeCap.butt,
                      lineHeight: 4.0,
                      percent: downloadProgress,
                      backgroundColor: REMOTE_COLOR,
                      progressColor: LOCAL_COLOR,
                      leading: Text("progress:",
                          style: new TextStyle(fontSize: 11.0)),
                      trailing: Text("${(downloadProgress * 100).round()}%",
                          style: new TextStyle(fontSize: 11.0)),
                    )),
                Container(
                    padding: defaultMargin(),
                    decoration: new BoxDecoration(
                        border: new Border(
                            top: new BorderSide(
                                width: 1.0, color: Colors.black12))),
                    child: Text(
                      postBody.text,
                      style: Theme.of(context).textTheme.bodyText2,
                    )),
                Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: RaisedButton(
                          onPressed: () {
                            print("Commenting on $forumId/$postId");
                            Navigator.pushNamed(context, "/comment",
                                arguments: PostArguments(forumId, postId));
                          },
                          child: Text(IntlMsg.of(context).comment),
                          color: Colors.purple[400],
                          textColor: Colors.white,
                        ))),
                (postComments != null)
                    ? new Container(
                        height: 300, child: postCommentsWidget(postComments))
                    : null
              ].where(notNull).toList()));
        }
      },
    );
  }
}

class DownloadWidget extends StatefulWidget {
  final models.PostBody postBody;
  final models.PostMetadata postMetadata;
  final String postId;
  final String forumId;
  DownloadWidget(this.postMetadata, this.postId, this.postBody, this.forumId);

  @override
  _DownloadWidgetState createState() => new _DownloadWidgetState();
}

class _DownloadWidgetState extends State<DownloadWidget> {
  Widget payloadActionIcon;
  Color statusColor;
  String hash;
  Future<Map> linksData;
  Future<String> filePath;

  @override
  void initState() {
//    statusColor = REMOTE_COLOR;
    if (widget.postBody.payloadLinks.length > 0) {
      hash = widget.postBody.payloadLinks[0].hash;
      final postBody = widget.postBody;
      final link = postBody.payloadLinks[0].link;
      linksData = rs.RsFiles.parseFilesLink(link);
      filePath = repo.getPathIfExists(hash);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final postMetadata = widget.postMetadata;
    final postId = widget.postId;
    final forumId = widget.forumId;

    return new IconButton(
        icon: Icon(Icons.cloud_download, color: REMOTE_COLOR),
        onPressed: () async {
          print("Requesting file: $linksData");
          linksData.then((data) {
            rs.RsFiles.requestFiles(data);
          });
          //TODO(nicoechaniz): verify that the file is effectively requested for Download before creating the bookmark post.
          String snackBarText;
          final bookmarksForumId =
              await repo.findOrCreateRepoForum("BOOKMARKS", rs.authIdentityId);
          print("Checking for bookmark in forum: $bookmarksForumId");
          final referred = "$forumId $postId";
          final bookmarkHeaders =
              await repo.getPostHeaders("BOOKMARKS", rs.authIdentityId);
          // TODO(nicoechaniz): find a more efficient way to do this
          List duplicates = bookmarkHeaders
              .where((i) => jsonDecode(i["mMsgName"])["referred"] == referred)
              .toList();
          if (duplicates.length > 0) {
            snackBarText = IntlMsg.of(context).contentAlreadyInLocalRepo;
          } else {
            final linkPostMetadata =
                models.PostMetadata.generateLinkPostMetadata(
                    postMetadata, postId, forumId);
            rs.RsGxsForum.createPost(bookmarksForumId,
                json.encode(linkPostMetadata.toJson()), "", rs.authIdentityId);
            snackBarText = IntlMsg.of(context).savingContentToLocalRepo;
          }
          final snackBar = SnackBar(
            content: Text(snackBarText),
            duration: Duration(seconds: 5),
            onVisible: () => {},
          );
          Scaffold.of(context).showSnackBar(snackBar);
        });
  }
}

class OpenPayloadWidget extends StatefulWidget {
  final Future<String> filePath;
  OpenPayloadWidget(this.filePath);

  @override
  _OpenPayloadWidgetState createState() => new _OpenPayloadWidgetState();
}

class _OpenPayloadWidgetState extends State<OpenPayloadWidget> {
  @override
  Widget build(BuildContext context) {
    return new IconButton(
        icon: Icon(Icons.open_in_new, color: LOCAL_COLOR),
        onPressed: () {
          widget.filePath.then((path) async {
            if (path != null) {
              print("Opening file");
              final openStatus = await OpenFile.open(path);
              print("Open file result: ${openStatus.message}");
            } else {}
          });
        });
  }
}

Future<List> getPostDetails(String forumId, String postId,
    [bool isLinkPost]) async {
  final details = await rs.RsGxsForum.getForumContent(forumId, [postId]);
  if (isLinkPost) {
    models.PostMetadata postMetadata =
        models.PostMetadata.fromJsonString(details[0]["mMeta"]["mMsgName"]);

    try {
      final realPostIds = postMetadata.referred.split(" ");
      final realPostDetails =
          await rs.RsGxsForum.getForumContent(realPostIds[0], [realPostIds[1]]);
      return realPostDetails;
    } catch (e) {
      print("Could not retrieve original post $e");
      return [];
    }
  }
  return details;
}

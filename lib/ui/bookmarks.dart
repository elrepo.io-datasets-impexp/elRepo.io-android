/// lib/ui/bookmarks.dart
part of ui;

class BookmarksScreen extends StatefulWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => BookmarksScreen(),
  );

  @override
  _BookmarksScreenState createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends State<BookmarksScreen> {
  Future<List> userPostHeaders;
  Future<List> postHeaders;

  @override
  void initState() {
    postHeaders = repo.getPostHeaders("BOOKMARKS", rs.authIdentityId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: postTeasersWidget(postHeaders, linkPosts: true),
    );
  }
}

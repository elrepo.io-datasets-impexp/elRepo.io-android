part of ui;


class OnBoarding extends StatefulWidget {

  @override
  _OnBoardingState createState() => _OnBoardingState();

}

class _OnBoardingState extends State<OnBoarding> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('creando cuenta'),
      ),
      body: IntroductionScreen(
        pages: [
          PageViewModel(
              bodyWidget: Center(
                child: Text("Redes comunitarias distribuidas",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey.shade500,shadows:[
                    Shadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(1,1),
                    )
                  ]),
                ),
              ),
              image:
              Image.asset("assets/img.jpg"),
              titleWidget: Text("Comparte cultura",
                style: TextStyle(
                    fontSize: 30
                ),)),
          PageViewModel(
              title: "Descubre amigos",
              bodyWidget: Center(
                child: Text("Intercambio local y global",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey.shade500,shadows:[
                    Shadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(1,1),
                    )
                  ]),
                ),
              ),
              image: Image.asset("assets/img1.jpg")
          ),
          PageViewModel(
              title: "Interactua",
              bodyWidget: Center(
                child: Text("Publica sin censura",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey.shade500,shadows:[
                    Shadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(1,1),
                    )
                  ]),
                ),
              ),
              image: Image.asset("assets/img2.jpg")
          ),
          PageViewModel(
              title: "Descentralizados",
              bodyWidget: Center(
                child: Text("Datos al alcance de todos",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey.shade500,shadows:[
                    Shadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(1,1),
                    )
                  ]),
                ),
              ),
              image: Image.asset("assets/img3.jpg")
          ),
          PageViewModel(
              title: "offline & online",
              bodyWidget: Center(
                child: Text("Menos ancho de banda",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey.shade500,shadows:[
                    Shadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 5,
                      offset: Offset(1,1),
                    )
                  ]),
                ),
              ),
              image: Image.asset("assets/img0.jpg")
          )
        ],
        onDone: (){
          print("done");
        },

        showNextButton: true,
        nextFlex: 1,
        dotsFlex: 1,
        skipFlex: 1,
        animationDuration: 1000,
        curve: Curves.fastOutSlowIn,
        dotsDecorator: DotsDecorator(
          spacing: EdgeInsets.all(5),
          activeColor: Color(0xffc5e1a5),
          activeSize: Size.square(12),
          size: Size.square(8),
          /*activeShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25)
            )*/
        ),


        next: Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              border: Border.all(color: Color(0xffc5e1a5), width: 2)),
          child: Center(
            child: Icon(
              Icons.navigate_next,
              size: 30,
              color: Color(0xffc5e1a5),
            ),
          ),
        ),
        done: Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
              color: Color(0xffc5e1a5),
              borderRadius: BorderRadius.circular(40),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.shade300,
                    blurRadius: 40,
                    offset: Offset(4,4)
                )
              ]
          ),
          child: Center(
            child: Text(
              "Hecho",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
        ),
      ),);
  }
}


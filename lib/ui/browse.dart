/// lib/ui/browse.dart
part of ui;

class BrowseScreen extends StatefulWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => BrowseScreen(),
  );
  @override
  _BrowseScreenState createState() => _BrowseScreenState();
}

class _BrowseScreenState extends State<BrowseScreen> {
  List _children;
  int _currentIndex = 0;

  @override
  void initState() {
    _children = [
      postTeasersWidget(repo.getPostHeaders()),
      tagsWidget(repo.getTagNames()),
      followingWidget(repo.getFollowedAuthorsDetails()),
    ];
    repo.syncForums();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.explore),
            title: Text('Discover'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.category), title: Text('Tags')),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_outline), title: Text('Following')),
        ],
      ),
    );
  }

  void onTabTapped(int tabIndex) {
    if (mounted) setState(() => _currentIndex = tabIndex);
  }
}

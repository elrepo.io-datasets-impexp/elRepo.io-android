part of ui;

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      /*
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      */

      body: new Container(
       // margin: defaultMargin(),
        child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/bg1.jpg"),
                      fit: BoxFit.cover,
                      alignment: Alignment.bottomCenter,
                    ),
                  ),
                ),
              ),
              new LoginForm()
            ]
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String password;
    return Form(
      key: _formKey,
      child: Expanded(
        flex: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Iniciar Sesión",
                      style: Theme.of(context).textTheme.display2,
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.lock,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: IntlMsg.of(context).password),
                        validator: (value) {
                          if (value.isEmpty) {
                            return IntlMsg.of(context).passwordCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => password = val,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: RaisedButton(
                    onPressed: () async {
                      final form = _formKey.currentState;
                      showLoadingDialog(context, IntlMsg.of(context).loggingIn);
                      if (form.validate()) {
                        form.save();
                        final responseStatus = await repo.login(password);

                        if (responseStatus != 0) {
                          final snackBar = SnackBar(
                            content: Text(IntlMsg.of(context).loginFailed),
                            duration: Duration(seconds: 6),
                          );
                          Navigator.pop(context); //pop dialog
                          Scaffold.of(context).showSnackBar(snackBar);
                          print("Bad login attempt.");
                          return;
                        }

                        await repo.prepareSystem();

                        Navigator.pop(context); //pop dialog
                        Navigator.pushReplacementNamed(context, '/main');
                      }
                    },
                    child: Center(child: Text(IntlMsg.of(context).login)),
                    color: LOCAL_COLOR,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

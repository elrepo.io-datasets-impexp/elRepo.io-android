import 'package:flutter/material.dart';

import '../../../ui.dart';

class TabNavigationItem {
  final Widget page;
  final Widget title;
  final Icon icon;

  TabNavigationItem({
    @required this.page,
    @required this.title,
    @required this.icon,
  });

  static List<TabNavigationItem> get items => [
    TabNavigationItem(
      page: BrowseScreen(),
      icon: Icon(Icons.home_filled),
      title: Text("Home"),
    ),
    TabNavigationItem(
      page: SettingsScreen(),
      icon: Icon(Icons.search_rounded),
      title: Text("Search"),
    ),
    TabNavigationItem(
      page: PublishScreen(),
      icon: Icon(Icons.add_circle_rounded),
      title: Text("Publish"),
    ),
    TabNavigationItem(
      page: BookmarksScreen(),
      icon: Icon(Icons.local_library_rounded),
      title: Text("Local"),
    ),
    TabNavigationItem(
      page: EditMyProfile(),
      icon: Icon(Icons.person),
      title: Text("Profile"),
    ),
  ];
}
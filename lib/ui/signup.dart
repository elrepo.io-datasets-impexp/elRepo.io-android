part of ui;

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      /*
      appBar: AppBar(
        title: Text(IntlMsg.of(context).signUp),
      ),
      */
      body: Container(
        //margin: defaultMargin(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/bg1.jpg"),
                    fit: BoxFit.cover,
                    alignment: Alignment.bottomCenter,
                  ),
                ),
              ),
            ),
            new SignUpForm(),
          ],
        ),
      ),
      /*new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new SignUpForm()]),
      ),
      */
    );
  }
}

class SignUpForm extends StatefulWidget {
  SignUpForm({Key key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String locationName;
    String password;
    return Form(
      key: _formKey,
      child: Expanded(
        flex: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Crear Cuenta",
                      style: Theme.of(context).textTheme.display2,
                    ),
                    FlatButton(
                      //onPressed: ()=> Navigator.of(context).push(BackupPage.route(),),
                      child: Text(
                        "Tengo un Backup",
                        style: Theme.of(context).textTheme.button,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.person,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: IntlMsg.of(context).username,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return IntlMsg.of(context).usernameCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => locationName = val,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.lock,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                            labelText: IntlMsg.of(context).password),
                        validator: (value) {
                          if (value.isEmpty) {
                            return IntlMsg.of(context).passwordCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => password = val,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 22),
                  child: RaisedButton(
                    onPressed: () async {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      final form = _formKey.currentState;
                      if (form.validate()) {
                        form.save();
                        //TODO show an status that let users know that their account is creating
                        showLoadingDialog(
                           context, IntlMsg.of(context).creatingAccount);
                        Navigator.of(context).pushNamed('/onboarding');


                        final location =
                            await repo.signUp(password, locationName);
                        await repo.login(password, location);
                        await repo.prepareSystem();

                        Navigator.pop(context); //pop dialog
                        Navigator.pushReplacementNamed(context, '/main');
                      }
                    },
                    child:
                        Center(child: Text(IntlMsg.of(context).createAccount)),
                    color: LOCAL_COLOR,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

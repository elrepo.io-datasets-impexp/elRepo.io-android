part of ui;

Widget followingWidget(Future<List> authorsDetails) {
  return FutureBuilder(
    future: authorsDetails,
    builder: (context, authorsSnapshot) {
      if (authorsSnapshot.hasError) {
        print('Error: ${authorsSnapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      }
      // TODO(nicoechaniz): there is some implementation error or a bug? which makes snapshot.data on the first run
      //  to be that of the previously showing view/tab, so we check the "type" (hacky solution)
      else if (authorsSnapshot.hasData == false ||
          authorsSnapshot.data.length == 0 ||
          !(authorsSnapshot.data[0] is Map &&
              (authorsSnapshot.data[0].containsKey("mNickname")))) {
        return loadingBox();
      } else if (authorsSnapshot.data[0]["mNickname"] == "") {
        return Text("el snapshot: $authorsSnapshot");
      } else {
        return ListView.builder(
          itemCount: authorsSnapshot.data.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: InkWell(
                  onTap: () {
                    print("tapped on ${authorsSnapshot.data[index]}");
                    Navigator.pushNamed(context, "/identityDetails",
                        arguments: IdentityArguments(
                            authorsSnapshot.data[index]["mId"]));
                  },
                  child: Text(authorsSnapshot.data[index]["mNickname"],
                      style: TextStyle(color: REMOTE_COLOR))),
            );
          },
        );
      }
    },
  );
}

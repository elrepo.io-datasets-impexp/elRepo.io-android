part of ui;

Widget tagsWidget(Future<List> tagNames) {
  return FutureBuilder(
    future: tagNames,
    builder: (context, tagsSnapshot) {
      if (tagsSnapshot.hasError) {
        print('Error: ${tagsSnapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      }
      // TODO(nicoechaniz): there is some implementation error or a bug? which makes snapshot.data on the first run
      //  to be that of the previously showing view/tab, so we check the type (hacky solution)
      else if (tagsSnapshot.hasData == false ||
          tagsSnapshot.data.length == 0 ||
          !(tagsSnapshot.data[0] is String)) {
        return loadingBox();
      } else {
        return ListView.builder(
          itemCount: tagsSnapshot.data.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: InkWell(
                  onTap: () {
                    print("tapped on ${tagsSnapshot.data[index]}");
                    Navigator.pushNamed(context, "/browsetag",
                        arguments: TagArguments(tagsSnapshot.data[index]));
                  },
                  child: Text(tagsSnapshot.data[index],
                      style: TextStyle(color: REMOTE_COLOR))),
            );
          },
        );
      }
    },
  );
}

part of ui;

class CommentScreen extends StatelessWidget {
  final PostArguments args;
  CommentScreen(this.args);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new CommentForm(args)]),
      ),
    );
  }
}

class _CommentData {
  models.PostMetadata metadata = new models.PostMetadata(
      markup: MarkupTypes.plain,
      contentType: ContentTypes.text,
      role: PostRoles.comment);
  models.PostBody mBody = new models.PostBody();
}

class CommentForm extends StatefulWidget {
  final PostArguments args;
  CommentForm(this.args, {Key key}) : super(key: key);

  @override
  _CommentFormState createState() => _CommentFormState();
}

class _CommentFormState extends State<CommentForm> {
  final _formKey = GlobalKey<FormState>();
  _CommentData _data = new _CommentData();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            maxLines: 3,
            decoration: InputDecoration(
                alignLabelWithHint: true,
                labelText: IntlMsg.of(context).comment),
            validator: (value) {
              _data.mBody.text = value;
              return null;
            },
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: RaisedButton(
                onPressed: () async {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  final form = _formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    showLoadingDialog(
                        context, IntlMsg.of(context).postingComment);

                    String msgBody = jsonEncode(_data.mBody);

                    msgBody = jsonEncode(_data.mBody);
                    if (_data.metadata.summary == null) {
                      _data.metadata.summary =
                          repo.summaryFromBody(_data.mBody.text);
                    }

                    String metadata = jsonEncode(_data.metadata);

                    final forumId = widget.args.forumId;
                    final msgId = repo.createComment(
                        forumId,
                        metadata,
                        msgBody,
                        widget.args.postId);

                    form.reset();
                    Navigator.pop(context); //pop dialog
                    Navigator.pop(context); //pop comment screen
                    // force refresh of post detail
                    Navigator.pushReplacementNamed(context, "/postdetails",
                        arguments: PostArguments(
                            widget.args.forumId, widget.args.postId, false));
                    print("post Commented with id: $msgId");
                  }
                },
                child: Text(IntlMsg.of(context).comment),
                color: Colors.purple[600],
                textColor: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}

Widget postCommentsWidget(Future<List> postsData) {
  return new FutureBuilder(
    future: postsData,
    builder: (context, postsSnapshot) {
      if (postsSnapshot.hasError) {
        print('Error: ${postsSnapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      }
      // TODO(nicoechaniz): there is some implementation error or a bug? which makes snapshot.data on the first run
      //  to be that of the previously showing view/tab, so we check the type (hacky solution)
      else if (postsSnapshot.hasData == false ||
          (postsSnapshot.data.length != 0 &&
              !(postsSnapshot.data[0] is Map<String, dynamic>))) {
        return loadingBox();
      } else if (postsSnapshot.data == null || postsSnapshot.data.length == 0) {
        return Visibility(visible: false, child: Text(""));
      } else {
        return ListView.builder(
          itemCount: postsSnapshot.data.length,
          itemBuilder: (context, index) {
            final data = postsSnapshot.data[index];
            final commentMsg = jsonDecode(data["mMsg"]);
            final commentMeta = data["mMeta"];
            final authorId = commentMeta['mAuthorId'];
            final authorName = repo.getAuthorName(authorId);

            return Card(
              elevation: 2.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  decoration: BoxDecoration(color: Colors.white),
                  child: FutureBuilder(
                      future: authorName,
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          print('Error: ${snapshot.error}');
                          return Center(
                            child: Text(IntlMsg.of(context).cannotLoadContent),
                          );
                        } else if (snapshot.hasData == false ||
                            snapshot.data.length == 0) {
                          return loadingBox();
                        } else {
                          final name = snapshot.data;
                          return ListTile(
                              leading: Container(
                                  padding: EdgeInsets.only(right: 5.0),
                                  decoration: new BoxDecoration(),
                                  child: Icon(Icons.person_outline,
                                      color: NEUTRAL_COLOR, size: 48)),
                              title: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(context, "/identityDetails",
                                      arguments: IdentityArguments(authorId));
                                },
                                child: Text(name,
                                    style: TextStyle(color: LOCAL_COLOR)),
                              ),
                              subtitle: Text(commentMsg["text"]));
                        }
                      })
                  ),
            );
          },
        );
      }
    },
  );
}

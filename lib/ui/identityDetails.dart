part of ui;

class IdentityDetailsScreen extends StatefulWidget {
  final IdentityArguments args;
  IdentityDetailsScreen(this.args);

  @override
  _IdentityDetailsScreenState createState() => _IdentityDetailsScreenState();
}

class _IdentityDetailsScreenState extends State<IdentityDetailsScreen> {
  Future<Map> identityDetails;
  Future<List> userPosts;
  Future<Map> userForum;
  bool alreadyFollowing = true;

  @override
  void initState() {
    identityDetails = repo.getIdDetails(widget.args.identityId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: identityDetailsWidget(),
    );
  }

  Widget identityDetailsWidget() {
    return Center(
      child: FutureBuilder(
          future: identityDetails,
          builder: (context, snapshot) {
            if (userForum == null) {
              userForum = repo.getUserForum(widget.args.identityId);
              userForum.then((forum) {
                //TODO(nicoechaniz): dirty implementation... without this check widget rebuilds constantly
                if (alreadyFollowing != (forum["mSubscribeFlags"] == 4)) {
                  setState(() {
                    alreadyFollowing = forum["mSubscribeFlags"] == 4;
                  });
                }
                if (alreadyFollowing) {
                  setState(() {
                    userPosts = repo.getForumMetadata(forum["mGroupId"]);
                  });
                  userPosts.then((p) => print("These are the user posts: $p"));
                }
              });
            }
            if (snapshot.hasError) {
              print('Error: ${snapshot.error}');
              return Center(
                child: Text(IntlMsg.of(context).cannotLoadContent),
              );
            } else if (snapshot.hasData == false || snapshot.data.length == 0) {
              return loadingBox();
            } else {
              final details = snapshot.data;

              return new ListView(
                  children: <Widget>[
                new ListTile(
                    leading: Container(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Icon(
                          Icons.person,
                          color: NEUTRAL_COLOR,
                          size: 48,
                        )),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          details["mNickname"],
                          style: TextStyle(
                            fontSize: 24,
                            color: Colors.lightGreen[400],
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    //TODO make the follow button interactive
                    subtitle: Visibility(
                      visible: !alreadyFollowing,
                      child: RaisedButton(
                        //color: !alreadyFollowing ? LOCAL_COLOR : REMOTE_COLOR,
                        onPressed: () {
                          followThisUser();
                          new Text(IntlMsg.of(context).following);
                        },
                        child: Text(IntlMsg.of(context).follow),
                        textColor: Colors.white,
                        color: LOCAL_COLOR,
                      ),
                      replacement: RaisedButton(
                        child: Text(IntlMsg.of(context).following),
                        disabledTextColor: Colors.white,
                        disabledColor: REMOTE_COLOR,
                      ),
                    )),
                SizedBox(
                  height: 60,
                ),
                (userPosts != null)
                    ? new Container(
                        height: 420,
                        child: postTeasersWidget(userPosts, linkPosts: true))
                    : null
              ].where(notNull).toList());
            }
          }),
    );
  }

  void followThisUser() {
    userForum.then((forum) {
      repo.subscribeToForum(forum);
      setState(() {
        alreadyFollowing = true;
      });
      print("Subscribed to $forum");
    });
  }
}
